# Tor Reproducer

This is a tool you can use to verify that the version of Tor
used by [Briar](https://briar.app) was built exactly from the public source code
and no modifications (such as backdoors) were made.

More information about these so called reproducible builds is available at
[reproducible-builds.org](https://reproducible-builds.org/).

The source code for this tool is available at
https://code.briarproject.org/briar/tor-reproducer

## How to use

Make sure the version of Tor you want to verify is included in `tor-versions.json`.

Verify that you have `docker` installed:

    docker --version

If this command does not work,
please [install Docker](https://docs.docker.com/install/)
and continue once it is installed.

### Using our pre-built image

If you trust that our pre-built Docker image was build exactly from *its* source,
you can use it for faster verification.
If not, you can read the next section to learn how to build the image yourself.
Then you are only trusting the official `debian:stable` image which is out of our control.

Otherwise, you can skip the next section and move directly to *Run the verification*.

### Building your own image

Check out the source repository:

    git clone https://code.briarproject.org/briar/tor-reproducer.git

Build our Docker image:

    docker build -t briar/tor-reproducer tor-reproducer

### Run the verification

To verify a specific version of Tor, run

    docker run briar/tor-reproducer:latest ./verify_tor.py [version]

Where `[version]` is the version of Tor you want to test, for example `0.3.3.6`.

You can find a list of versions in Tor's
[source code repository](https://gitweb.torproject.org/tor.git/refs/).
Just remove the `tor-` from `tor-0.3.3.6`.

If you leave out `[version]` it will build the latest version
that was registered in `tor-versions.json`.

In case there is an issue with the verification of an old build,
this *might* be caused by an update of the container.
You can try to use the original container by running:

    docker run briar/tor-reproducer:[version] ./verify_tor.py [version]

There should be a tag with the name `[version]` in this repository
that you could be used to reproduce the old container.
Note that this will not work if the issue is caused by an updated Debian package.

### Historical changes

Versions before 0.4.7.13-1 used a different format for the published artifacts.
Please use the version of tor-reproducer tagged `0.4` to reproduce those.

The Tor 0.3.x series had a different build system than the 0.4.x series.
Please use a version of tor-reproducer with a tag that starts with `0.3` to
reproduce those.

### Only build Tor

To build a specific version of Tor, run

    docker run briar/tor-reproducer:latest ./build_tor.py [version]

### Upstream build system

This repository uses the upstream Tor build system for building macOS
binaries. The `tor-versions.json` file defines the configuration for that
system via the `upstream` stanza.

We maintain our own
[fork](https://code.briarproject.org/briar/tor-browser-build) of the
[tor-browser-build](https://gitlab.torproject.org/tpo/applications/tor-browser-build)
repository so that we're able to define which Tor browser version and
exact upstream commit/tag we're building our Tor binaries from and
additionally have the ability to apply patches.
This makes it for example possible to build Tor binaries with a higher
version number than yet released in a Tor browser release.
We maintain a branch that's based on one of the upstream branches such
as `maint-12.0` but appends a tor version (i.e. `maint-12.0-tor-0.4.7.14`).
Then we create an annotated tag such as `tor-0.4.7.14` from that branch
and use that tag name in the `tor-versions.json` configuration file.
